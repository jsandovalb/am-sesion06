package pe.academiamoviles.amsesion06.session

import android.content.Context
import android.content.SharedPreferences

class AppPrefs(val context: Context) {

    private lateinit var sharedPref: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    init {
        sharedPref = context
            .getSharedPreferences("MiSharedPref_SuperApp", Context.MODE_PRIVATE)
        editor = sharedPref.edit()
    }

    var estaLogeado: Boolean
        get() = sharedPref.getBoolean("ESTA_LOGEADO", false)
        set(value) {
            editor.putBoolean("ESTA_LOGEADO", value)
            editor.apply()
        }

}