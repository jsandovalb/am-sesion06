package pe.academiamoviles.amsesion06.ui.contenido.detalle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_detalle_usuario.*
import pe.academiamoviles.amsesion06.R

class DetalleFragment : Fragment() {

    val argumentos : DetalleFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detalle_usuario, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        println("usuario id a consultar: ${argumentos.usuarioId}")



        ivRetroceder.setOnClickListener {

        }
    }

}