package pe.academiamoviles.amsesion06.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_login.*
import pe.academiamoviles.amsesion06.R
import pe.academiamoviles.amsesion06.session.AppPrefs
import pe.academiamoviles.amsesion06.ui.main.MainActivity

class LoginActivity : AppCompatActivity() {

    val appPrefs by lazy { AppPrefs(this) }
    val viewModel by lazy { ViewModelProvider(this).get(LoginViewModel::class.java) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
    }

    private fun init() {
        btnIngresar.setOnClickListener {
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()

            if (email.isNotEmpty() && password.isNotEmpty()) {

                viewModel.login(email, password)

            } else {
                Toast.makeText(this, "Faltan campos", Toast.LENGTH_LONG).show()
            }

        }

        viewModel.loginEvent.observe(this, Observer { loginResponse ->
            if (loginResponse.success) {
                // login ha sido correcto
                appPrefs.estaLogeado = true
                abrirMain()

            } else {
                // hubo un error
                println("mensaje de error: ${loginResponse.mensaje}")

                Toast.makeText(this, loginResponse.mensaje, Toast.LENGTH_LONG).show()
            }
        })

        validarSiEstaLogeado()
    }

    private fun validarSiEstaLogeado() {
        val estaLogeado = appPrefs.estaLogeado
        println("logeado? $estaLogeado")

        if (estaLogeado) {
            abrirMain()
        }

    }

    private fun abrirMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}

/*
if (email == "prueba@prueba.com" && password == "123") {

    // guardar en shared preferences
    appPrefs.estaLogeado = true

    abrirMain()

} else {
    Toast.makeText(this, "Credenciales inválidas", Toast.LENGTH_LONG).show()
}
 */