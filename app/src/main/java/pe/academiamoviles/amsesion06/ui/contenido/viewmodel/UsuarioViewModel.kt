package pe.academiamoviles.amsesion06.ui.contenido.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.academiamoviles.amsesion06.api.ApiClient
import pe.academiamoviles.amsesion06.api.response.UsuarioResponse

class UsuarioViewModel : ViewModel() {

    val usuarios = MutableLiveData<List<UsuarioResponse>>()
    val detalleUsuario = MutableLiveData<UsuarioResponse>()

    fun obtenerListadoUsuarios() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                val listadoDeUsuarios = ApiClient.api.obtenerUsuario()
                usuarios.postValue(listadoDeUsuarios)

            } catch (ex: Exception) {

            }
        }
    }

    fun obtenerDetalleUsuario(usuarioId: String) {

    }


}