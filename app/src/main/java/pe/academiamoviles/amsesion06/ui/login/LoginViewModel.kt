package pe.academiamoviles.amsesion06.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import pe.academiamoviles.amsesion06.api.ApiClient
import pe.academiamoviles.amsesion06.api.request.LoginRequest
import pe.academiamoviles.amsesion06.api.response.LoginResponse

class LoginViewModel : ViewModel() {

    val loginEvent = MutableLiveData<LoginResponse>()

    fun login(email: String, password: String) {

        CoroutineScope(Dispatchers.IO).launch {
            val request = LoginRequest(email, password)
            try {
                val loginResponse = ApiClient.api.login(request)
                loginEvent.postValue(loginResponse)
            } catch (ex: Exception) {

            }
        }

    }

}