package pe.academiamoviles.amsesion06.ui.contenido.lista

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_lista_usuarios.*
import pe.academiamoviles.amsesion06.R
import pe.academiamoviles.amsesion06.api.response.UsuarioResponse
import pe.academiamoviles.amsesion06.ui.contenido.viewmodel.UsuarioViewModel

class ListaFragment : Fragment(), UsuariosAdapter.ClickListener {

    val viewModel by lazy { ViewModelProvider(this).get(UsuarioViewModel::class.java) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_lista_usuarios, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        rvPersonas.layoutManager = LinearLayoutManager(context)

        viewModel.usuarios.observe(viewLifecycleOwner, Observer { usuarios ->
            val adapter = UsuariosAdapter(usuarios, this)
            rvPersonas.adapter = adapter
        })

        viewModel.obtenerListadoUsuarios()
    }

    override fun seleccionarUsuario(usuario: UsuarioResponse) {
        println("usuario seleccionado: $usuario")

        val action = ListaFragmentDirections.actionListaFragmentToDetalleFragment(usuario.id)
        findNavController().navigate(action)
    }

}