package pe.academiamoviles.amsesion06.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import pe.academiamoviles.amsesion06.R
import pe.academiamoviles.amsesion06.session.AppPrefs
import pe.academiamoviles.amsesion06.ui.login.LoginActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        btnSalir.setOnClickListener {
            val appPrefs = AppPrefs(this)
            appPrefs.estaLogeado = false

            abrirLogin()
        }
    }

    private fun abrirLogin() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

}
