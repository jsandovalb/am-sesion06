package pe.academiamoviles.amsesion06.ui.contenido.lista

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.item_usuario.view.*
import pe.academiamoviles.amsesion06.R
import pe.academiamoviles.amsesion06.api.response.UsuarioResponse

class UsuariosAdapter(
    private val listaUsuarios: List<UsuarioResponse>,
    private val clickListener: ClickListener
) : RecyclerView.Adapter<UsuariosAdapter.UsuariosHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsuariosHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_usuario, parent, false)
        return UsuariosHolder(view, clickListener)
    }

    override fun onBindViewHolder(holder: UsuariosHolder, position: Int) {
        val usuario = getUsuario(position)
        holder.mostrarDatos(usuario)
    }

    override fun getItemCount(): Int = listaUsuarios.size
    private fun getUsuario(position: Int): UsuarioResponse = listaUsuarios[position]

    class UsuariosHolder(private val view: View, private val clickListener: ClickListener) : RecyclerView.ViewHolder(view) {
        fun mostrarDatos(usuario: UsuarioResponse) {
            // completar datos de cada usuario
            view.tvNombres.text = usuario.nombres ?: "--"
            view.tvTitulo.text = usuario.titulo ?: "--"
            view.ivAvatar.load(usuario.imagenUrl)

            view.setOnClickListener { clickListener.seleccionarUsuario(usuario) }
        }
    }

    interface ClickListener {
        fun seleccionarUsuario(usuario: UsuarioResponse)
    }
}