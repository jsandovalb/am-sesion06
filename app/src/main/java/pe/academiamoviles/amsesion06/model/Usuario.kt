package pe.academiamoviles.amsesion06.model

data class Usuario(
    val id: String = "",
    val nombres: String? = null,
    val email: String? = null,
    val imagenUrl: String? = null
)