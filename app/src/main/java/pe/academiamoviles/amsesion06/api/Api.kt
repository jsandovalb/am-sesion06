package pe.academiamoviles.amsesion06.api

import pe.academiamoviles.amsesion06.api.request.LoginRequest
import pe.academiamoviles.amsesion06.api.response.LoginResponse
import pe.academiamoviles.amsesion06.api.response.UsuarioResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface Api {

    @POST("usuarios/login")
    suspend fun login(
        @Body request: LoginRequest
    ): LoginResponse

    @GET("usuarios")
    suspend fun obtenerUsuario(): List<UsuarioResponse>

    // nuevo
    @GET("usuarios/detalles/{usuarioId}")
    suspend fun obtenerDetalleUsuario(
        @Path("usuarioId") id: String
    ): LoginResponse  // datos obtenidos similar al login response

}