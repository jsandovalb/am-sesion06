package pe.academiamoviles.amsesion06.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("success") @Expose
    val success: Boolean = false,

    @SerializedName("mensaje") @Expose
    val mensaje: String? = "",

    @SerializedName("usuario") @Expose
    val usuario: UsuarioResponse? = null
)