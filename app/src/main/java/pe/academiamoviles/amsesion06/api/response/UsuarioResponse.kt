package pe.academiamoviles.amsesion06.api.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UsuarioResponse(
    @SerializedName("id") @Expose
    val id: String = "",

    @SerializedName("nombres") @Expose
    val nombres: String? = null,

    @SerializedName("titulo") @Expose
    val titulo: String? = null,

    @SerializedName("imagenUrl") @Expose
    val imagenUrl: String? = null
)