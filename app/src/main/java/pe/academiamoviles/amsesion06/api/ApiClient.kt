package pe.academiamoviles.amsesion06.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    val api: Api by lazy {
        // okhttp intercept <------- okhttp

        val retrofit = Retrofit.Builder()
            .baseUrl("https://academia-usuarios.herokuapp.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        retrofit.create(Api::class.java)
    }

}